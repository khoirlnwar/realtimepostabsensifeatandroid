package dev.onprojek.com.realtimeabsensiapp;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.work.Constraints;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import dev.onprojek.com.realtimeabsensiapp.models.LocationRangeResponseModel;
import dev.onprojek.com.realtimeabsensiapp.models.UserAbsensiResponseModel;
import dev.onprojek.com.realtimeabsensiapp.models.UserLoginResponseModel;
import dev.onprojek.com.realtimeabsensiapp.models.UserPutModel;
import dev.onprojek.com.realtimeabsensiapp.services.RealtimeAbsensiInterface;
import dev.onprojek.com.realtimeabsensiapp.services.RealtimeAbsensiWorker;
import dev.onprojek.com.realtimeabsensiapp.services.RetrofitApiClient;
import dev.onprojek.com.realtimeabsensiapp.utils.Preferences;
import dev.onprojek.com.realtimeabsensiapp.utils.TimeConstraints;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DashboardActivity extends AppCompatActivity {

    private MaterialButton mButtonLogout, mButtonAbsenMasuk, mButtonAbsenKeluar, mButtonNavtoupdate;
    private TextView mTextviewInformasiUser, mTextviewInformasiTanggal;

    private Retrofit retrofit;
    private FusedLocationProviderClient mFusedLocation;

    private RealtimeAbsensiInterface realtimeAbsensiInterface;
    private WorkManager mWorkManager;

    private boolean absenMasuk;
    private String jamMasuk, jamKeluar;

    private int idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);


        // initialComponents();
        getDetailUser();
        initialBackgroundWork();


//
//        double[] userLocation = new double[2];
//
//        userLocation[0] = 99.1;
//        userLocation[1] = 100.1;

//        getDetailUser(userLocation);
        // initialAutomaticAbsensi();
    }

    public void initialAutomaticAbsensi() {
        startAbsenProcess();
    }

    public void initialBackgroundWork() {
        mWorkManager = WorkManager.getInstance(getApplicationContext());
        Log.d("TAG", "initialBackgroundWork: " + Preferences.getBackgroundJob(getApplicationContext()));
        if (!Preferences.getBackgroundJob(getApplicationContext()))
            setPeriodicSendLog();
    }

    private void setPeriodicSendLog() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(RealtimeAbsensiWorker.class, 15, TimeUnit.MINUTES, 5, TimeUnit.MINUTES)
                    .setConstraints(Constraints.NONE)
                    .build();

            OneTimeWorkRequest oneTimeWorkRequest = new OneTimeWorkRequest.Builder(RealtimeAbsensiWorker.class).build();

            mWorkManager.enqueue(oneTimeWorkRequest);
            mWorkManager.enqueue(periodicWorkRequest);

            Toast.makeText(getApplicationContext(), "Periodic send log started", Toast.LENGTH_SHORT).show();
            Preferences.setBackgroundJob(getApplicationContext(), true);
        }
    }

    public void initialComponents() {
        mButtonAbsenMasuk = (MaterialButton) findViewById(R.id.absenMasukBtn);
        mButtonAbsenKeluar = (MaterialButton) findViewById(R.id.absenKeluarBtn);

        mButtonLogout = (MaterialButton) findViewById(R.id.logoutBtn);
        mButtonNavtoupdate = (MaterialButton) findViewById(R.id.navToUpdate);

        mTextviewInformasiTanggal = (TextView) findViewById(R.id.informasiTanggal);
        mTextviewInformasiUser = (TextView) findViewById(R.id.informasiUser);

        mButtonLogout.setOnClickListener(logoutListener);

        mButtonAbsenMasuk.setOnClickListener(absensiListener);
        mButtonAbsenKeluar.setOnClickListener(absensiListener);

        Log.e(TAG, "initialComponents: absen masuk " + absenMasuk);

        if (absenMasuk) {
            mButtonAbsenMasuk.setEnabled(true);
            mButtonAbsenKeluar.setEnabled(false);
        } else {
            mButtonAbsenMasuk.setEnabled(false);
            mButtonAbsenKeluar.setEnabled(true);
        }


        mButtonNavtoupdate.setOnClickListener(navListener);


        mTextviewInformasiTanggal.setText("Today: " + TimeConstraints.getCurrentDate());
        mTextviewInformasiUser.setText(Preferences.getNama(getApplicationContext()));
    }

    private final View.OnClickListener navListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // Navigation to update user
            Intent intent = new Intent(getApplicationContext(), UpdateUserActivity.class);
            startActivity(intent);
        }
    };

    private final View.OnClickListener logoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            // 1. Clear all preferences
            Preferences.clearPrefs(getApplicationContext());

            // start new activity
            Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    };


    private final View.OnClickListener absensiListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startAbsenProcess();
        }
    };

    private void startAbsenProcess() {
//        if (!TimeConstraints.checkContraintsIn() && !TimeConstraints.checkConstraintsOut()) {
//            // Absensi gagal
//            // Diluar batas waktu absensi
//            Toast.makeText(DashboardActivity.this, "Absensi gagal, di luar waktu absen", Toast.LENGTH_SHORT).show();
//        } else {
//            absenLokasi();
//        }

        absenLokasi();
    }

    @SuppressLint("MissingPermission")
    private void absenLokasi() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Do: permission request
            requestPermissions();
            return;
        }
        mFusedLocation.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    String message = "Lokasi anda telah dikonfirmasi " + "Latitude: " + latitude + " Longitude: " + longitude;
                    Toast.makeText(DashboardActivity.this, message, Toast.LENGTH_SHORT).show();

                    double[] userLocation = new double[2];

                    userLocation[0] = latitude;
                    userLocation[1] = longitude;

//                    userLocation[0] = -8.114084;
//                    userLocation[1] = 111.796544;

                    getLocation(userLocation);
                } else
                    Log.d("TAGLOC", "onSuccess: Gagal dapat lokasi");
            }
        });
    }

    private void requestPermissions() {
        // REQUEST FOR PERMISSIONS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Toast.makeText(DashboardActivity.this, "MAKE PERMISSIONS", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
            }, 44);
        }
    }


    private void getLocation(double[] userLocation) {
        Call<List<LocationRangeResponseModel>> call = realtimeAbsensiInterface.getLocationRange();

        call.enqueue(new Callback<List<LocationRangeResponseModel>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<LocationRangeResponseModel>> call, Response<List<LocationRangeResponseModel>> response) {
                if (response.code() != 200) {
                    // Get code
                    // Error here
                    Toast.makeText(DashboardActivity.this, "Absensi gagal. Gagal mendapatkan lokasi dari server", Toast.LENGTH_SHORT).show();
                } else {
                    double latMax = response.body().get(0).getLatitudeMax();
                    double latMin = response.body().get(0).getLatitudeMin();

                    double longMax = response.body().get(0).getLongitudeMax();
                    double longMin = response.body().get(0).getLongitudeMin();

                    // Reversed latitude
                    boolean latInRange = userLocation[0] <= latMin && userLocation[0] >= latMax;
                    boolean lonInRange = userLocation[1] >= longMin && userLocation[1] <= longMax;

                    if (latInRange && lonInRange) {

                        LocalDateTime localDateTime = LocalDateTime.now();
                        int hours = localDateTime.getHour();
                        int minutes = localDateTime.getMinute();
                        int second = localDateTime.getSecond();

                        // Log.d(TAG, "onResponse: " + parseT(hours) + ":" + parseT(minutes) + ":" + parseT(second));

                        String time = parseT(hours) + ":" + parseT(minutes) + ":" + parseT(second);

                        if (absenMasuk)
                            absenProses(userLocation, time, jamKeluar, idUser);
                        else
                            absenProses(userLocation, jamMasuk, time, idUser);


                    } else {
                        Toast.makeText(DashboardActivity.this, "Absensi gagal. Di luar area absensi", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<LocationRangeResponseModel>> call, Throwable t) {
                // Error here
                Toast.makeText(DashboardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getDetailUser() {

        retrofit = RetrofitApiClient.getRetrofitClientInstance();
        mFusedLocation = LocationServices.getFusedLocationProviderClient(this);

        realtimeAbsensiInterface = retrofit.create(RealtimeAbsensiInterface.class);

        Context context = getApplicationContext();
        String nip = Preferences.getNip(context);

        Call<List<UserLoginResponseModel>> call = realtimeAbsensiInterface.getDetailUser(nip);

        call.enqueue(new Callback<List<UserLoginResponseModel>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<UserLoginResponseModel>> call, Response<List<UserLoginResponseModel>> response) {
                if (response.code() != 200) {
                    Log.d(TAG, "onResponse: something error get detail user " + response.code());
                } else {
                    assert response.body() != null;
                    idUser = response.body().get(0).getId();

                    jamMasuk = response.body().get(0).getJam_masuk();
                    jamKeluar = response.body().get(0).getJam_keluar();


                    LocalDateTime localDateTime = LocalDateTime.now();
                    int dd = localDateTime.getDayOfMonth();

                    String tanggal = response.body().get(0).getTanggal();
                    String[] splitted = tanggal.split("-");

                    Log.e(TAG, "onResponse: dd: " + dd + " " + splitted[2]);

                    if (!splitted[2].equalsIgnoreCase(String.valueOf(dd))) {
                        absenMasuk = true;
                    } else {
                        absenMasuk = jamMasuk.equals("00:00:00");
                    }


                    initialComponents();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<UserLoginResponseModel>> call, @NonNull Throwable t) {

            }
        });
    }

    private String parseT(int time) {
        String hh = "";
        if (time < 10) hh = "0" + time;
        else hh = "" + time;

        return hh;
    }

    private void absenProses(double[] location, String jamMasuk, String jamKeluar, int id) {

        Context context = getApplicationContext();

        String name = Preferences.getNama(context);
        String nip = Preferences.getNip(context);
        String pangkat = Preferences.getPangkat(context);
        String jabatan = Preferences.getJabatan(context);
        String latitude = String.valueOf(location[0]);
        String longitude = String.valueOf(location[1]);
        String alamat = Preferences.getAlamat(context);
        String email = Preferences.getEmail(context);

        final RequestBody rNama = RequestBody.create(MediaType.parse("text/plain"), name);
        final RequestBody rNip = RequestBody.create(MediaType.parse("text/plain"), nip);
        final RequestBody rPangkat = RequestBody.create(MediaType.parse("text/plain"), pangkat);
        final RequestBody rJabatan = RequestBody.create(MediaType.parse("text/plain"), jabatan);
        final RequestBody rLatitude = RequestBody.create(MediaType.parse("text/plain"), latitude);
        final RequestBody rLongitude = RequestBody.create(MediaType.parse("text/plain"), longitude);
        final RequestBody rId = RequestBody.create(MediaType.parse("text/plain"), "" + id);
        final RequestBody rJM = RequestBody.create(MediaType.parse("text/plain"), jamMasuk);
        final RequestBody rJK = RequestBody.create(MediaType.parse("text/plain"), jamKeluar);

        Call<UserAbsensiResponseModel> call = realtimeAbsensiInterface.absen(rNama, rPangkat, rNip, rLatitude, rLongitude,
                rJabatan, rId, rJM, rJK);
        call.enqueue(new Callback<UserAbsensiResponseModel>() {
            @Override
            public void onResponse(Call<UserAbsensiResponseModel> call, Response<UserAbsensiResponseModel> response) {

                if (response.code() == 200) {
                    Toast.makeText(DashboardActivity.this, "Absensi berhasil", Toast.LENGTH_SHORT).show();

                    Call<UserPutModel> callPut = realtimeAbsensiInterface.user(name, pangkat, nip, jabatan, alamat, email, longitude, latitude, jamMasuk, jamKeluar, "tanggal");
                    callPut.enqueue(new Callback<UserPutModel>() {
                        @Override
                        public void onResponse(Call<UserPutModel> call, Response<UserPutModel> response) {
                            if (response.code() == 200) {
                                Toast.makeText(DashboardActivity.this, "Update User berhasil", Toast.LENGTH_SHORT).show();
                                getDetailUser();
                            }
                        }

                        @Override
                        public void onFailure(Call<UserPutModel> call, Throwable t) {

                        }
                    });


                } else {
                    Toast.makeText(DashboardActivity.this, "Absensi gagal " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserAbsensiResponseModel> call, Throwable t) {
                Toast.makeText(DashboardActivity.this, "Absensi gagal " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}