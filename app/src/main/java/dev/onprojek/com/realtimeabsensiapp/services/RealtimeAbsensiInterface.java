package dev.onprojek.com.realtimeabsensiapp.services;

import java.util.List;

import dev.onprojek.com.realtimeabsensiapp.models.LocationRangeResponseModel;
import dev.onprojek.com.realtimeabsensiapp.models.RealtimeAbsensiResponseModel;
import dev.onprojek.com.realtimeabsensiapp.models.ResponseUbahPassword;
import dev.onprojek.com.realtimeabsensiapp.models.UserAbsensiResponseModel;
import dev.onprojek.com.realtimeabsensiapp.models.UserLoginResponseModel;
import dev.onprojek.com.realtimeabsensiapp.models.UserPutModel;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RealtimeAbsensiInterface {

    @GET("login")
    Call<List<UserLoginResponseModel>> login(@Query("nip") String nip, @Query("password") String password);

    @GET("user")
    Call<List<UserLoginResponseModel>> getDetailUser(@Query("nip") String nip);

    @GET("LocationRange")
    Call<List<LocationRangeResponseModel>> getLocationRange();

    @Multipart
    @POST("absensi")
    Call<UserAbsensiResponseModel> absen(@Part("nama") RequestBody rNama,
                                         @Part("pangkat") RequestBody rPangkat,
                                         @Part("nip") RequestBody rNip,
                                         @Part("latitude") RequestBody rLatitude,
                                         @Part("longitude") RequestBody rLongitude,
                                         @Part("jabatan") RequestBody rJabatan,
                                         @Part("id") RequestBody rId,
                                         @Part("jam_masuk") RequestBody rJamMasuk,
                                         @Part("jam_keluar") RequestBody rJamKeluar

    );

    @FormUrlEncoded
    @PUT("user")
    Call<UserPutModel> user(@Field("nama") String nama,
                            @Field("pangkat") String pangkat,
                            @Field("nip") String nip,
                            @Field("jabatan") String jabatan,
                            @Field("alamat") String alamat,
                            @Field("email") String email,
                            @Field("longitude") String longitude,
                            @Field("latitude") String latitude,
                            @Field("jam_masuk") String jamMasuk,
                            @Field("jam_keluar") String jamKeluar,
                            @Field("tanggal") String tanggal
    );

    @Multipart
    @POST("realtime")
    Call<RealtimeAbsensiResponseModel> realtime(@Part("nip") RequestBody rNip,
                                                @Part("nama") RequestBody rNama,
                                                @Part("longitude") RequestBody rLongitude,
                                                @Part("latitude") RequestBody rLatitude);


    @FormUrlEncoded
    @PUT("user")
    Call<ResponseUbahPassword> updatePassword(@Field("nip") String nip,
                                              @Field("password") String password);

}
